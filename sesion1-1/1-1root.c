#include <stdio.h>
//Add the required #include directive
#include <math.h>

int main()
{
	double num = 9.0;
	double root = sqrt(num);
	printf("The square root of %.2f is %.2f", num, root);
	return 0;
}
